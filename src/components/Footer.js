import React, { Component } from 'react';

export class Footer extends Component {
    render() {
        return (
            <div>
                <footer>
                    <img className="logo" src={require('../assets/img/logo-white.svg')} alt="Work Remote" />
                    <div className="phone">
                        Phone: <a href="tel:+40733577513">+40 733-577-513</a>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer
