import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export class Navigation extends Component {

    constructor(props) {
        super(props);
        this.state = {
          isOpen: false
        };

        this.toggleMenu = this.toggleMenu.bind(this);

      }
    
    toggleMenu () {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render () {
        return (
            <header>
                <div className="container-fluid">
                    <nav>
                        <div className="item-nav links">
                            <NavLink to="/jobs">Jobs</NavLink>
                        </div>
                        <div className="item-nav item-logo">
                            <img className="logo" src={require('../assets/img/logo.svg')} alt="Work Remote" />
                        </div>
                        <div className="item-nav last">
                            <NavLink exact to="/">Despre</NavLink>
                        </div>
                    </nav>
                    <div className="nav-mobile">
                        <div className="item-nav item-logo">
                            <img className="logo" src={require('../assets/img/logo.svg')} alt="Work Remote" />
                        </div>
                        <div className="burger-menu" onClick={this.toggleMenu}>
                            <FontAwesomeIcon icon={['fas', 'bars']} size="2x" />
                        </div>
                        {!this.state.isOpen ? null :
                        <div className="menu-dropdown" id="menu-dropdown">
                            <ul>
                                <li><NavLink exact to="/">Despre</NavLink></li>
                                <li><NavLink to="/jobs">Jobs</NavLink></li>
                            </ul>
                        </div>
                        }
                    </div>
                </div>
            </header>
        )
    }
}

export default Navigation