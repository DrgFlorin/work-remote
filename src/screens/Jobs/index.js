import React, { Component } from 'react';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Footer from '../../components/Footer';

export class index extends Component {

    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          jobs: [],
          numberOfJobs: null
        };
      }

    async componentDidMount() {
      let jobs;
        await fetch("https://remotive.io/api/remote-jobs")
          .then(res => res.json())
          .then(
            (result) => {
              let favoritesFromStorage = JSON.parse(localStorage.getItem('favorites'));
              jobs = result.jobs.map(job => {
                let newArray = Object.assign({}, job)
                  if (favoritesFromStorage && favoritesFromStorage.some(e => e.id === job.id)) {
                    newArray.favorit = true;
                    return newArray;
                  } else {
                    newArray.favorit = false;
                    return newArray;
                  }
              })
              this.setState({
                isLoaded: true,
                jobs: jobs,
                numberOfJobs: result["job-count"]
              });

            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }
      manipulateSessionStorage (id) {
        let favorites = this.state.jobs.filter(e => e.favorit === true);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      }

      addToFavorite (jobId) {
        let newArray = this.state.jobs.map(job => {
          if (job.id !== jobId) return job;
          if (!job.favorit) {
            job.favorit = true;
            this.manipulateSessionStorage(jobId);
            return job;
          } else {
            job.favorit = false;
            this.manipulateSessionStorage(jobId);
            return job;
          }
          
        })
        this.setState({
          jobs: newArray
        })
      }
    render() {
        return (
            <div id="jobs">
                <div className="spacer"></div>
                <div className="container">
                  <div className="row">
                    <div className="col-12">
                      <h4>Number of jobs:&nbsp;

                        {(this.state.numberOfJobs ) ? this.state.numberOfJobs : 
                          <FontAwesomeIcon icon={['fas', 'spinner']} spin />
                        }
                      
                      </h4>
                    </div>
                    {!this.state.isLoaded ? 
                    <div>
                      <h4>Loading jobs <FontAwesomeIcon icon={['fas', 'spinner']} spin size="1x"/></h4>
                    </div> : 
                      this.state.jobs.map(job => {
                        return (
                          <div className="col-lg-3 col-md-4" key={job.id}>
                            <div className="box">
                              <div className="title">
                                <a href={job.url} target="_blank" rel="noopener noreferrer">{job.title}</a>
                              </div>
                              <div className="date">
                                Posted on: {moment(job.publication_date).format("DD.MM.YYYY hh:mm")}
                              </div>
                              <div className="type">
                                Type: {job.job_type}
                              </div>
                              <div className="company-name">
                                Company: {job.company_name}
                              </div>
                              <div className="location">
                                Where : {job.candidate_required_location}
                              </div>
                              <div className="icons">
                                <div className="favorite" onClick={() => {this.addToFavorite(job.id)}}>
                                {!job.favorit 
                                ? <FontAwesomeIcon icon={['fas', 'heart']} color="lightGray" />
                                : <FontAwesomeIcon icon={['fas', 'heart']} color="red" />
                                }
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                  </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default index
