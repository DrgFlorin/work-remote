import React, { Component } from 'react';
import Footer from '../../components/Footer';

export class index extends Component {
    render() {
        return (
            <div>
                <section id="slide-show">
                    <h2 className="not-show">Lorem ipsum dolor</h2>
                    <div className="slideShowContainer">
                        <div className="slider">
                            <div className="slide1" />
                            <div className="slide2" />
                            <div className="slide3" />
                        </div>
                    </div>
                </section>

                <section id="content">
                    <h2 className="not-show">Lorem ipsum dolor</h2>
                    <div className="container">
                        <div className="row">
                        <div className="content-item left col-md-6">
                            <img src={require('../../assets/img/content-2.jpg')} alt="content-image1" />
                        </div>
                        <div className="content-item right col-md-6">
                            <p>Work from home!</p>
                            <img src={require('../../assets/img/content-1.jpg')} alt="content-image2" />
                        </div>
                        </div>
                        <div className="description">
                            <p>
                                Poti aplica la orice job dorit din confortul casei tale. Cel mai relaxant job este cel facut de
                                acasa. Fara stres si fara limite, iar castigurile vin pe masura. Cauta cu incredere pe site-ul
                                nostru jobul dorit.
                            </p>
                        </div>
                    </div>
                </section>

                <div className="content-background" />
                <Footer />
                
            </div>
        )
    }
}

export default index
