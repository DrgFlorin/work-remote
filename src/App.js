import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './assets/stylesheet/main.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faFacebookF, faTwitter, faInstagram,  } from '@fortawesome/free-brands-svg-icons'


// Pages
import Landing from './screens/Landing';
import Jobs from './screens/Jobs';

// Nav
import Navigation from './components/Navigation';

// Errors 
import Error404 from './screens/404';

library.add(fab, fas, faFacebookF, faTwitter, faInstagram)

function App() {
  return (
    <div className="App">
      <BrowserRouter>
          <div>
          <Navigation />
            <Switch>
              <Route path="/" exact component={Landing} />
              <Route path="/jobs" component={Jobs} />
              <Route path='*' component={Error404} />
            </Switch>
            
          </div>
        </BrowserRouter>
    </div>
  );
}

export default App;
